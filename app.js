const express = require('express')
const http = require('http')
const app = express();
const server = http.createServer(app)
const bodyParser = require('body-parser')
const { log } = require('console');
const masterRoute = require('./routes/masterRoute.js')
const registerRoute = require('./routes/registerRoutes.js')
const settingMerchantRoutes = require('./routes/settingMerchantRoutes.js')
const loginRoutes = require('./routes/loginRoutes.js')
const customerRoutes = require('./routes/customerRoutes.js')
const transaksiRoutes = require('./routes/transaksiRoutes.js')
const pembayaranRoutes = require('./routes/pembayaranRoutes.js')
// const fileupload = require("express-fileupload");

const PORT = process.env.PORT || 8000;
app.use(express.json())
app.use(bodyParser.urlencoded({
    extended: false
}));
// app.use(fileupload());  
app.use('/master',masterRoute)
app.use('/register',registerRoute)
app.use('/setting',settingMerchantRoutes)
app.use('/login',loginRoutes)
app.use('/transaksi',transaksiRoutes)
app.use('/customer',customerRoutes)
app.use('/bayar',pembayaranRoutes)


server.listen(PORT, () => {
    console.log(`server connected in port http://localhost:${PORT}`)
})


