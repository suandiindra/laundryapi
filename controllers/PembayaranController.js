const DBLaundry = require('../config')
const mysql = require('mysql2');
const respone = require('../utils/respone')
const fs = require('fs');
const { join } = require('path');
const promisify = require('util.promisify');
const mv = promisify(fs.rename);
const { v4: uuidv4 } = require('uuid');
const { request } = require('http');


module.exports = {
    bayar: async function (req, res) {
        const { merchant_id, nama_bank, nomor_rekening, nominal_pay } = req.body
        try {
            const request = DBLaundry.promise();
            const file = req.file;

            let unik_id = uuidv4()
            if (!file) {
                res.send({
                    status: false,
                    message: `No file uploaded`,req
                });
            }
            let sel1 = `select * from payment_term pt `;
            let qr1 = await request.query(sel1);
            let bayar = qr1[0][0].biaya
            let hari = qr1[0][0].jumlah_hari

            let cekDulu = `select * from payment p where isconfirm is null and merchant_id = '${merchant_id}'`
            let qrCek = await request.query(cekDulu);
            if(qrCek[0].length > 0){
                return respone("500", `Pembayaran anda sedang menunggu konfirmasi..`)
            }

            // if (nominal_pay % bayar !== 0) {
            //     return respone("500", `Pembayaran anda ${nominal_pay} Harus kelipatan ${bayar}`)
            // }
            let bagiBayar = nominal_pay / bayar
            let sel2 = `select *,date_format(masa_berlaku,'%Y-%m-%d') as masa from m_merchant where m_merchant_id = '${merchant_id}'`;
            let qr2 = await request.query(sel2);


            let masa_berlaku = qr2[0][0].masa
            hari = parseInt(hari) * parseInt(bagiBayar)

            let path = './assets/' + file.filename;
            let insert = `insert into payment
                    (payment_id,createdate ,merchant_id 
                    ,nama_bank ,nomor_rekening ,nominal_pay ,isactive ,last_date 
                    ,icoming_date ,jumlah_hari ,bukti)
                    values 
                    ('${unik_id}',now(),'${merchant_id}'
                    ,'${nama_bank}' ,'${nomor_rekening}' ,'${nominal_pay}' ,1 ,'${masa_berlaku}' 
                    ,date_add('${masa_berlaku}',interval +${hari} day) ,${hari},'${path}')`

            await request.query(insert);

            return respone("200","Berhasil")
        } catch (error) {
            return respone("700",error)
        }
    },
    top: async function (req, res) {
        try {
            const {m_merchant_id} = req.query
            console.log(m_merchant_id);
            const request = DBLaundry.promise();
            let sel = `select * from payment_term`
            let result = await request.query(sel);

            let notif = result[0][0].jumlah_hari_notif
            console.log(notif);
            let sel2 = `select *,datediff(masa_berlaku ,now()) batas 
            ,case when datediff(masa_berlaku ,now()) <= ${notif} then 'Y'
            else 'N' end notif from 
            m_merchant where m_merchant_id = '${m_merchant_id}'`
            let result2 = await request.query(sel2);
            console.log(sel2);

            let hasil = {
                result1 : result[0],
                result2 : result2[0]
            }

            return respone("200", hasil)
        } catch (error) {
            console.log(error);
            return respone("500", error)
        }
    },
    konfirmasiPembayaran: async function(req,res){
        try {
            const {payment_id,val} = req.body
            const request = DBLaundry.promise();
            let sel = `select * from payment p where payment_id = '${payment_id}'`
            // console.log(sel);
            let result = await request.query(sel);


            let hari = result[0][0].jumlah_hari;
            let merchant_id = result[0][0].merchant_id;
            let update1 = `update payment set isconfirm = '${val == "Y" ? 1 : 0}', updatedate = now() where payment_id = '${payment_id}'`
            await request.query(update1);

            let update2 = `update m_merchant set masa_berlaku = date_add(masa_berlaku,interval +${hari} day)
            ,updatedate = now()
            where m_merchant_id = '${merchant_id}'`;
            await request.query(update2);


            return respone("200", result[0])
        } catch (error) {
            console.log(error);
            return respone("500", error)
        }
    }
}

async function upd(_file, filename) {
    await _file.mv('./assets/' + filename, async function (err) {
        if (err) {
            console.log(err);
            return respone("500", "Gagal Upload")
        } else {
            return respone("200", "Berhasil upload")
        }
    })
    // return respone("200", "Berhasil upload")
    // return filename
}