const DBLaundry = require('../config')
const mysql = require('mysql2');
const respone = require('../utils/respone')

module.exports = {
    master : async function(req,res){
        try {
            const {nama,table,filterx} = req.query
            const request = DBLaundry.promise();

            let filter = ``
            if(nama){
                filter = ` and nama = '${nama}'`
            }

            let query = `select * from ${table} where 1=1 ${filter} ${filterx ? filterx : ''}`

            console.log(query,filterx);
            let result = await request.query(query);

            return respone("200",result[0])
        } catch (error) {
            return respone("500",error)
        }
    }
}
