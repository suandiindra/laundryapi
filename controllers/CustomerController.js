const DBLaundry = require('../config')
const mysql = require('mysql2');
const respone = require('../utils/respone')

module.exports = {
    find: async function (req, res) {
        try {
            const { field,merchant_id } = req.query
            const request = DBLaundry.promise();

            let filter = ``
            if (field) {
                filter = ` and nama like '%${field}%' or nomor_tlp like '%${field}%'`
            }

            let sel = `select * from m_customer where 1=1 and m_merchant_id = '${merchant_id}' ${filter} 
            order by nama asc`
            let result = await request.query(sel);
            return respone("200", result[0])
        } catch (error) {
            return respone("500", error)
        }
    },
    editCustomer : async function(req,res){
        const {nama,nomor_tlp,alamat,catatan,jenis_pelanggan,m_customer_id} = req.body
        const request = DBLaundry.promise();
        try {
            let upd = `update m_customer set nama = '${nama}',nomor_tlp = '${nomor_tlp}'  
            ,alamat = '${alamat}',catatan = '${catatan}'
            where m_customer_id = '${m_customer_id}'`;
            let result = await request.query(upd);
            if(result){
                let sel = `select * from m_customer where m_customer_id = '${m_customer_id}'`
                let result = await request.query(sel);
                return respone("200", result[0])
            }else{
                return respone("200",upd)
            }
        } catch (error) {
            return respone("500", error)
        }
    },
    daftar : async function(req,res){
        try {
            const {nama,nomor_tlp,alamat,catatan,jenis_pelanggan,merchant_id} = req.body
            const request = DBLaundry.promise();

            let filter = ``
            if (nama) {
                filter = ` and nomor_tlp = '${nomor_tlp}' `
            }

            let sel = `select * from m_customer where 1=1 ${filter}`
            let result = await request.query(sel);
            let act = ``
            if(result[0].length > 0){
                let m_customer_id = result[0][0].m_customer_id
                act = `update m_customer set 
                ${nama ? `nama = '${nama}'` : `nama = nama`}, 
                ${nomor_tlp ? `nomor_tlp = '${nomor_tlp}'` : `nomor_tlp = nomor_tlp`}, 
                ${alamat ? `alamat = '${alamat}'` : `alamat = alamat`},
                ${catatan ? `catatan = '${catatan}'` : `catatan = catatan`},
                ${jenis_pelanggan ? `jenis_pelanggan = '${jenis_pelanggan}'` : `jenis_pelanggan = jenis_pelanggan`}
                where m_customer_id = '${m_customer_id}'`
            }else{
                act = `insert into m_customer (m_customer_id,nama,nomor_tlp,alamat,isactive,catatan,jenis_pelanggan,createdate,merchant_id)
                values (uuid(),'${nama}','${nomor_tlp}','${alamat}',1,${catatan ? `'${catatan}'` : `''`},'Medium',now(),'${merchant_id}')`
            }

            let hasil = await request.query(act);
            if(hasil){
                return respone("200", hasil)
            }else{
                return respone("500", act)
            }
           
        } catch (error) {
            return respone("500", error)
        }
    }
}