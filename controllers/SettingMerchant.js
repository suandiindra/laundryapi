const DBLaundry = require('../config')
const mysql = require('mysql2');
const respone = require('../utils/respone')
const { v4: uuidv4 } = require('uuid');

module.exports = {
    pilihproduk: async function (req, res) {
        try {
            const { products } = req.body
            const request = DBLaundry.promise();
            const unik_id = uuidv4()
            let m_merchant_id = products[0].m_merchant_id

            let sel1 = `select * from m_merchant where m_merchant_id = '${m_merchant_id}'`
            let dsSel1 = await request.query(sel1)
            let m_merchant_setting_id = dsSel1[0][0].merchant_setting_id


                let m_produk_id = products[0].m_produk_id
                let nama_produk = products[0].nama_produk
                let isactive = products[0].isactive

                let upd = `update setting_merchant set isactive = ${isactive} 
                where m_produk_id = '${m_produk_id}' and merchant_setting_id = '${m_merchant_setting_id}'`

                let result = await request.query(upd)

                // let insert = `insert into setting_merchant
                // (setting_merchant_id ,merchant_setting_id 
                // ,m_produk_id ,nama_produk ,isstok ,harga ,hitungan_harga ,waktu_proses 
                // ,isactive ,createdate) values
                // (uuid() ,'${unik_id}' 
                // ,'${m_produk_id}' ,'${nama_produk}','${products[i].isstok ? 1 : 0}' ,'${products[i].harga}' 
                // ,'${products[i].hitungan_harga}' ,${products[i].waktu_proses}
                // ,1 ,now())`
                // console.log(upd);

                // let updateHeader = `update m_merchant set merchant_setting_id = '${unik_id}' 
                // where m_merchant_id = '${m_merchant_id}'`
                // let result = await request.query(updateHeader)

            return respone("200", {
                pesan: upd,
                objek: result
            })
        
        } catch (error) {
            console.log(error);
            return respone("500", {
                pesan: error,
                objek: null
            })
        }
    },
    listProduk: async function (req, res) {
        try {
            const {merchant_setting_id} = req.query
            const request = DBLaundry.promise();
            let filter = ``

            let pt = `select * from payment_term pt `
            let limitHari = await request.query(pt)
            limitHari = limitHari[0][0].jumlah_hari_notif

            if(merchant_setting_id){
                filter = ` and merchant_setting_id = '${merchant_setting_id}'`
            }
            let sql = `select mp.*,b.m_produk_id as id
            ,coalesce(b.isactive ,0) as isactive
            ,masa_berlaku,dateDIFF(masa_berlaku,now())sisa_hari
            ,case when dateDIFF(masa_berlaku,now()) <= ${limitHari} then '1' else '0' end tampil
            from m_produk mp 
            left join setting_merchant b on b.m_produk_id 
            = mp.m_produk_id where 1=1 ${filter}`
            let result = await request.query(sql);
            return respone("200", {
                pesan: `Success Get Data`,
                objek: result[0]
            })
        } catch (error) {
            return respone("500", {
                pesan: error,
                objek: null
            })
        }
    },
    settProduk : async function(req,res){
        try {
            const { m_produk_id, merchant_setting_id,harga,waktu_proses,hitungan_harga,isstok } = req.body
            const request = DBLaundry.promise();
            let query = `update setting_merchant set isstok = '${isstok}',harga = '${harga}',hitungan_harga = '${hitungan_harga}'
                        ,waktu_proses = '${waktu_proses}' where m_produk_id = '${m_produk_id}' 
                        and merchant_setting_id = '${merchant_setting_id}'`

                        console.log(query);
            let rsult = await request.query(query)
            if(rsult){
                return respone("200", {
                    pesan: `Berhasil`,
                    objek: rsult
                })
            }
            return respone("500", {
                pesan: `Gagal`,
                objek: query
            })
            
        } catch (error) {
            return respone("500", {
                pesan: error,
                objek: null
            })
        }
    },
    productById : async function(req,res){
        try {
            const { m_produk_id, merchant_setting_id} = req.body
            const request = DBLaundry.promise();
            let query = `select * from setting_merchant where merchant_setting_id = '${merchant_setting_id}' 
            and m_produk_id = '${m_produk_id}'`
            console.log(query);
            let rsult = await request.query(query)
            if(rsult){
                return respone("200", {
                    pesan: query,
                    objek: rsult[0][0]
                })
            }
            return respone("500", {
                pesan: `Gagal`,
                objek: query
            })
            
        } catch (error) {
            return respone("500", {
                pesan: error,
                objek: null
            })
        }
    }
}