const DBLaundry = require('../config')
const mysql = require('mysql2');
const { v4: uuidv4 } = require('uuid');
const respone = require('../utils/respone')

module.exports = {
    regiter: async function (req, res) {
        try {
            const { obj,objMerchant } = req.body
            const request = DBLaundry.promise();
            const unik_id = uuidv4()
            const unik_merchant = uuidv4()
            let nama = obj.nama
            let email = obj.email
            let phone = obj.phone
            let provinsi = obj.provinsi
            let alamat = obj.alamat
            let kota = obj.kota
            let password = objMerchant.password

            let ins = `insert into m_user (m_user_id,nama,email,phone,provinsi,kota,alamat,isactive,createdate,pwd)
            values ('${unik_id}','${nama}','${email}','${phone}'
            ,'${provinsi}','${kota}','${alamat}',0,now(),'${password}')`;

            // console.log(ins);
            let result = await request.query(ins);
            if (result) {
                let insert = `insert into m_merchant
                (m_merchant_id,m_user_id,nama_merchant,phone ,alamat ,isactive,createdate,merchant_setting_id,masa_berlaku)
                values (uuid(),'${unik_id}','${objMerchant.nama_merchant}','${objMerchant.phone}' 
                ,'${objMerchant.alamat}' ,1,now(),'${unik_merchant}',DATE_ADD(now(),INTERVAL +30 day))`
                let resultMercahant = await request.query(insert)

                let insert2 = `insert into setting_merchant
                (setting_merchant_id ,merchant_setting_id 
                ,m_produk_id ,nama_produk ,isstok ,harga ,hitungan_harga ,waktu_proses 
                ,isactive ,createdate,masa_berlaku) 
                select uuid() ,'${unik_merchant}' 
                ,m_produk_id,nama_produk,0,0 
                ,0 ,1
                ,0 ,now(),DATE_ADD(now(),INTERVAL +30 day) from m_produk mp`
                let result = await request.query(insert2)

                return respone("200", {
                    pesan: `Berhasil insert`,
                    objek: obj
                })
            }else{
                return respone("500", {
                    pesan: `Gagal Insert`,
                    objek: obj
                })
            }

        } catch (er) {
            return respone("500", er)

        }
    },
    find: async function(req,res){
        try {
            const { id } = req.query
            const request = DBLaundry.promise();
            let filter = ``

            if(id){
                filter = ` and m_user_id = '${id}' `
            }

            let find = `select * from m_user where 1=1 ${filter}`

            console.log(find);
            let result = await request.query(find);
            return respone("200", result[0])
            
        } catch (error) {
            return respone("200", error)
            
        }
    },
    edituser: async function(req,res){
        try {
            const { obj } = req.body
            console.log(obj);
            const request = DBLaundry.promise();

            let m_user_id = obj.m_user_id
            let nama = obj.nama
            let email = obj.email
            let phone = obj.phone
            let provinsi = obj.provinsi
            let alamat = obj.alamat
            let kota = obj.kota

            let ins = ` update m_user set nama = '${nama}'
                        ,email = '${email}'
                        ,phone = '${phone}'
                        ,provinsi = '${provinsi}'
                        ,kota = '${kota}'
                        ,alamat = '${alamat}'
                        ${obj.pwd ? `,pwd = 'obj.pwd'`:''}
                        ${obj.isactive ? `,isactive = '${obj.isactive}'`:''}
                        where m_user_id = '${m_user_id}' `;
            let result = await request.query(ins);
            if (result) {
                return respone("200", {
                    pesan: `Berhasil Update`,
                    objek: obj
                })
            }else{
                return respone("500", {
                    pesan: `Gagal Insert`,
                    objek: obj
                })
            }

        } catch (er) {
            return respone("200", er)

        }
    },
    merchant: async function(req,res){
        try {
            const { obj } = req.body
            const request = DBLaundry.promise();
            let insert = `insert into m_merchant
            (m_merchant_id,m_user_id,nama_merchant,phone ,alamat ,isactive,createdate )
            values (uuid(),'${obj.m_user_id}','${obj.nama_merchant}','${obj.phone}' 
            ,'${obj.alamat}' ,1,now())`
            let result = await request.query(insert)
            if(result){
                return respone("200", {
                    pesan: `Transaksi Berhasil`,
                    objek: obj
                })
            }else{
                return respone("200", {
                    pesan: `Transaksi Gagal`,
                    objek: obj
                })
            }
        }catch(e){
            return respone("500", e)
        }
    }
}