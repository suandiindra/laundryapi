const DBLaundry = require('../config')
const mysql = require('mysql2');
const respone = require('../utils/respone')
const { v4: uuidv4 } = require('uuid');


module.exports = {
    input : async function(req,res){
        const {m_customer_id ,nomorTlp
            ,m_merchant_id ,nama ,alamat 
            ,m_produk_id, nama_produk  ,merchant_setting_id
            ,qty,catatan} = req.body
        
        
        try {
            const request = DBLaundry.promise();
            let unik_id = uuidv4()
            const today = new Date();
            const yyyy = today.getFullYear();
            let mm = today.getMonth() + 1; // Months start at 0!
            let dd = today.getDate();

            if (dd < 10) dd = '0' + dd;
            if (mm < 10) mm = '0' + mm;

            const formattedToday = mm + '' + yyyy.toString().substring(2);


            let selId = `select count(*)itungan from transaksi t where m_merchant_id = '${m_merchant_id}'
            and date_format(createdate,'%Y-%m') =  date_format(now(),'%Y-%m')`
            let ds = await request.query(selId);
            let itungan = ds[0][0].itungan;
            itungan = ('0000' + itungan).slice(-4)
            itungan = formattedToday+'-'+itungan
            console.log(itungan);


            // return respone("500","Produk tidak ditemukan...")

            if(m_customer_id == "" || !m_customer_id){
                let cust = `insert into m_customer (m_customer_id,nama,nomor_tlp,alamat,isactive,catatan,jenis_pelanggan,createdate,m_merchant_id)
                values ('${unik_id}','${nama}','${nomorTlp}','${alamat}',1,'','Medium',now(),'${m_merchant_id}')`
                console.log(cust);
                await request.query(cust);
            }else{
                unik_id = m_customer_id;
            }
            let qcek1 = `select * from setting_merchant sm 
            where m_produk_id = '${m_produk_id}' and merchant_setting_id = '${merchant_setting_id}'`

            let qs = await request.query(qcek1);
            if(qs[0].length == 0){
                return respone("500","Produk tidak ditemukan...")
            }
            let dSet= qs[0][0]
            let satuan = dSet.hitungan_harga
            let harga = dSet.harga
            let amount = harga * qty
            let hari = dSet.waktu_proses


            let insert = `insert into transaksi 
            (transaksi_id,createdate ,m_customer_id 
            ,m_merchant_id ,nama ,alamat ,isactive ,finishDate 
            ,m_produk_id, nama_produk  ,satuan 
            ,qty ,amount ,kode_status ,status ,catatan,harga,no_struk)
            values (uuid(),now() ,'${unik_id}' 
            ,'${m_merchant_id}' ,'${nama}' ,'${alamat}' ,1 ,date_add(now(),interval ${hari} day) 
            ,'${m_produk_id}', '${nama_produk}' ,'${satuan}' 
            ,${qty},${amount},'WT1' ,'Order Created' ,'${catatan}',${harga},'${itungan}')`

            console.log(insert);

            let result = await request.query(insert);
            if(result){
                return respone("200","Berhasil")
            }
            return respone("500",insert)
        } catch (error) {
            console.log(error);
            return respone("500",error)
        }
    },
    find : async function(req,res){
        const {m_merchant_id,kode_status,key} = req.body
        try {
            const request = DBLaundry.promise();
            
            let filter1 = ``
            if(m_merchant_id){
                filter1 = filter1+` and t.m_merchant_id = '${m_merchant_id}' `
            }
            if(kode_status){
                if(kode_status == 1){
                    filter1 = filter1+` and kode_status = 'WT1' `
                }else if(kode_status == 2){
                    filter1 = filter1+` and kode_status = 'WT2' `
                }else if(kode_status == 3){
                    filter1 = filter1+` and kode_status = 'WT3' `
                }
            }

            let filterKey = ``
            if(key){
                filterKey = ` and (b.nama like '%${key}%' or nomor_tlp like '%${key}%' or t.catatan like '%${key}%' or t.no_struk like '%${key}%')`
            }
            let sel = `select t.*,date_format(t.createdate,'%d %M') as dibuat,b.nama 
            ,date_format(t.finishDate,'%d %M') as selesai
            ,b.alamat ,b.nomor_tlp
            ,date_format(t.createdate,'%d') as tanggal
            ,SUBSTR(date_format(t.createdate,'%M'),1,3) as bulan
            ,date_format(t.finishDate,'%d') as tanggal_selesai
            ,SUBSTR(date_format(t.finishDate,'%M'),1,3) as bulan_selesai
            ,date_format(t.actual_finish_date ,'%d') as tanggal_aktual
            ,SUBSTR(date_format(t.actual_finish_date,'%M'),1,3) as bulan_aktual
            from transaksi t 
            left join m_customer b on b.m_customer_id = t.m_customer_id 
            where t.isactive = 1 ${filter1} ${filterKey} order by t.createdate desc`;
            console.log(sel);
            let resSel = await request.query(sel);
            
            resSel = resSel[0]
            return respone("200",resSel)
        } catch (error) {
            return respone("500",error)
        }   
    },
    update : async function(req,res){
        const {transaksi_id,status} = req.body
        try {
            const request = DBLaundry.promise();
            let q = ``;
            if(status == "Selesai"){
                q = `update transaksi set actual_finish_date = now(), kode_status = 'WT2',status = 'Finish Worked'
                ,updatetime = now()
                where transaksi_id = '${transaksi_id}'`
            }else if(status == "Batal"){
                q = `update transaksi set isactive = 0 ,updatetime = now()
                ,kode_status = 'WT0', status = 'Cancel' where transaksi_id = '${transaksi_id}'`
            }else if(status == "Picking"){
                q = `update transaksi set updatetime = now()
                ,kode_status = 'WT3', status = 'Picked' where transaksi_id = '${transaksi_id}'`
            }

            let hasil = await request.query(q)
            if(hasil){
                return respone("200","Berhasil")
            }
            return respone("500","Gagal")
        }catch(e){
            return respone("500",e)
        }
    },
    loads : async function(req,res){
        // console.log(req);
        const {m_merchant_id,period1,period2} = req.query
        try {
            const request = DBLaundry.promise();

            let objek = {}
            let sel1 = `select 
            coalesce(sum(amount),0) as amount,
            count(*) as jml
            from transaksi t where isactive = 1
            and date_format(createdate ,'%Y-%m-%d') =  date_format(now() ,'%Y-%m-%d')
            and m_merchant_id  = '${m_merchant_id}'`
            let res1 = await request.query(sel1)
            let dataToday = res1[0]

            let sumaryPending = `select count(*) as jml,sum(amount) as amount from transaksi t where isactive = 1
            and date_format(createdate ,'%Y-%m') =  date_format(now() ,'%Y-%m')
            and kode_status in ('WT1','WT2')
            and m_merchant_id  = '${m_merchant_id}'`
            let resPending = await request.query(sumaryPending)

            let sumaryTaken = `select count(*) as jml,coalesce(sum(amount),0) as amount from transaksi t where isactive = 1
            and date_format(createdate ,'%Y-%m') =  date_format(now() ,'%Y-%m')
            and kode_status in ('WT3')
            and m_merchant_id  = '${m_merchant_id}'`
            let resTaken = await request.query(sumaryTaken)

            let summary = `select sum(case when kode_status = 'WT1' then 1 else 0 end) as proses 
            ,sum(case when kode_status = 'WT2' then 1 else 0 end) as selesai
            from transaksi t 
            where t.isactive  = 1
            and m_merchant_id  = '${m_merchant_id}'`
            let sum = await request.query(summary)

            console.log(summary);

            objek = {... objek,dataToday : dataToday
                    ,dataPending : resPending[0]
                    ,dataTaken : resTaken[0],dataSummary : sum[0]}

            return respone("200",objek)
        } catch (error) {
            console.log(error);
            return respone("500",error)
        }
    }
}