const express = require('express')
const respone = require('../utils/respone.js')
const route = express.Router();
const LoginController = require('../controllers/LoginController')
route.use(express.urlencoded({ extended: false }));

route.post('/',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let rp = await LoginController.login(req,res)
        return res.send(rp) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})

module.exports = route