const express = require('express')
const respone = require('../utils/respone.js')
const route = express.Router();
const SettingMerchant = require('../controllers/SettingMerchant')
route.use(express.urlencoded({ extended: false }));

route.post('/pilihproduk',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let rp = await SettingMerchant.pilihproduk(req,res)
        return res.send(rp) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})
route.get('/listProduk',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let rp = await SettingMerchant.listProduk(req,res)
        return res.send(rp) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})
route.post('/productbyid',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let rp = await SettingMerchant.productById(req,res)
        return res.send(rp) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})
route.post('/settproduk',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let rp = await SettingMerchant.settProduk(req,res)
        return res.send(rp) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})
module.exports = route
