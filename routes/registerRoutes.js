const express = require('express')
const respone = require('../utils/respone.js')
const route = express.Router();
const registerController = require('../controllers/RegisterController')
route.use(express.urlencoded({ extended: false }));

route.post('/',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let rp = await registerController.regiter(req,res)
        return res.send(rp) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})
route.get('/',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let rp = await registerController.find(req,res)
        return res.send(rp) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})
route.post('/edituser',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let rp = await registerController.edituser(req,res)
        return res.send(rp) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})
route.post('/merchant',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let rp = await registerController.merchant(req,res)
        return res.send(rp) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})
module.exports = route