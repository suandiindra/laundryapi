const express = require('express')
const respone = require('../utils/respone.js')
const route = express.Router();
const PembayaranController = require('../controllers/PembayaranController')
const multer = require('multer');
route.use(express.urlencoded({ extended: false }));
const path = require("path");
const fs = require('fs');

const diskStorage = multer.diskStorage({
    destination: function (req, file, cb) {
      var dir = `./assets/${req.body.merchant_id}`;
      if(!fs.existsSync(dir)){
        fs.mkdirSync(dir);
      }
      cb(null, dir);
    },
    filename: function (req, file, cb) {
      cb(null,file.originalname);
    },
});
const upload = multer({storage:diskStorage})  

route.post('/',upload.single("foto"),async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let rp = await PembayaranController.bayar(req,res)
        return res.send(rp) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})
route.get('/top',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let rp = await PembayaranController.top(req,res)
        return res.send(rp) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})
route.post('/konfirmasiPembayaran',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let rp = await PembayaranController.konfirmasiPembayaran(req,res)
        return res.send(rp) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})

module.exports = route