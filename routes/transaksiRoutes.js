const express = require('express')
const respone = require('../utils/respone.js')
const route = express.Router();
const TransaksiController = require('../controllers/TransaksiController')
route.use(express.urlencoded({ extended: false }));

route.post('/input',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let rp = await TransaksiController.input(req,res)
        return res.send(rp) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})
route.post('/find',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let rp = await TransaksiController.find(req,res)
        return res.send(rp) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})
route.get('/loads',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let rp = await TransaksiController.loads(req,res)
        return res.send(rp) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})
route.post('/update',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let rp = await TransaksiController.update(req,res)
        return res.send(rp) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})



module.exports = route