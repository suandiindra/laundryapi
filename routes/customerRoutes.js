const express = require('express')
const respone = require('../utils/respone.js')
const route = express.Router();
const CustomerController = require('../controllers/CustomerController')
route.use(express.urlencoded({ extended: false }));

route.get('/find',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let rp = await CustomerController.find(req,res)
        return res.send(rp) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
}),
route.post('/daftar',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let rp = await CustomerController.daftar(req,res)
        return res.send(rp) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
}),
route.post('/edit',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let rp = await CustomerController.editCustomer(req,res)
        return res.send(rp) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})

module.exports = route